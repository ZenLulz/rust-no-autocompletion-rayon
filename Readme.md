## Environment
- **IntelliJ Rust plugin version**: 0.2.106.2135-192
- **Rust toolchain version**: stable-x86_64-pc-windows-gnu rustc 1.38.0 (625451e37 2019-09-23)
- **IDE name and version**: CLion 2019.2.3
- **Operating system**: Windows 10 Pro x64 1906

## Problem description
The autocompletion sometime does not yield any result. This example demonstrates a concrete scenario using the crate `rayon`. The function `par_iter()` does not proposes any suggestion, while the code is working, The screenshot below details the issue:

![issue](https://bitbucket.org/ZenLulz/rust-no-autocompletion-rayon/raw/822f34c3f114159188ed54c0acae7621bb1237b3/issue.png "")

## Steps to reproduce
Clone `https://ZenLulz@bitbucket.org/ZenLulz/rust-no-autocompletion-rayon.git`. Open the project in the environment defined above. Write a dot (.) after the `par_iter()` function, in `main.rs`. Notice that no autocompletion is given.
